<?php
require_once 'configurator.php';
require_once 'tools.php';
require_once 'model.php';
require_once 'presentation.php';
require_once 'app.php';

$cfg = new Configurator (
    'config.json',
    array(
        'base_path' => null,
        'configurable' => true,
        'mongos' => array(
            'localhost' => array(
                'conn' => 'mongodb://localhost/',
                'db' => '*',
                'color' => 'green'
            )
        )
    ));
$cfg->persist();

list ($base_path, $url) = tune_urls ($cfg->base_path);
list ($dest, $args) = route (Application::get_routes(), $url);
$model = new MomoModel ($cfg->mongos);
$ctrl = new Application ($model, $base_path, $url, $cfg, $args);
$ctrl->run ($dest, $args);
