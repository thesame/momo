function idecode (s) {
    return "Date: " + (new Date (parseInt (s.substr (0, 8), 16)*1000)).toString()
    + "; Machine: " + s.substr (8, 6)
    + "; PID: " + parseInt (s.substr (14, 4), 16)
    + "; Count: " + parseInt (s.substr (18, 6), 16);
}

function syntaxHighlight(json, id_prefix) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    if (!syntaxHighlight.bigre)
    {
        syntaxHighlight.bigre = new RegExp (''
            +'(?:"((?:\\\\u[a-zA-Z0-9]{4}|\\\\[^u]|[^\\\\"])*)"(?:\\s*:)?)|'
            +'(\\b(?:true|false)\\b)|'
            +'(\\bnull\\b)|'
            +'(-?\\d+(?:\\.\\d*)?(?:[eE][+\\-]?\\d+)?)',
            "g");
    }

    var match;
    var res = '';
    var previdx = 0;
    var prev = null;

    while (match = syntaxHighlight.bigre.exec (json))
    {
        var m0 = match[0];

        res += json.substr (previdx, match.index - previdx);
        previdx = match.index + m0.length;

        var comment = null;
        var cls;
        var s = match[1];
        if (s) //string
        {
            if (m0.slice(-1) == ':')
                cls = (s[0] == '$') ? 'extended' : 'key';
            else
            {
                cls = 'string';
                if (prev === '$oid')
                {
                    m0 = '<a href="'+id_prefix+s+'">'+s+'</a>';
                    comment =
                        "Date: " + (new Date (parseInt (s.substr (0, 8), 16)*1000)).toString()
                        + "; Machine: " + s.substr (8, 6)
                        + "; PID: " + parseInt (s.substr (14, 4), 16)
                        + "; Count: " + parseInt (s.substr (18, 6), 16);
                }
            }
            prev = s;
        }
        else if (match[2])
            cls = 'boolean';
        else if (match[3])
            cls = 'null';
        else if (match[4])
        {
            cls = 'number';
            if (prev === '$date')
                comment = new Date (parseInt (m0)).toString();
        }

        res += '<span class="js' + cls + '">' + m0 + '</span>'
            + (comment ? ('<span class="jscomment"> // '+comment+'</span>') : '');
    }
    res += json.substr (previdx, json.length - previdx);
    return res;
}

function highlight_start (id_prefix) {
    var records = document.getElementById ('records');
    if (!records)
        return;

    records.style.visibility = "hidden";

    var els = records.getElementsByTagName ('pre').toArray();
    var c = els.length;
    var alljs = '';

    for (i = 0; i < c; ++i)
    {
        var e = els[i];
        var json = e.innerHTML;
        if (window.JSON && /\bunpretty\b/.test (e.className))
            json = JSON.stringify (JSON.parse (json), undefined, "    ");
        alljs += '<pre>'+syntaxHighlight (json, id_prefix + '/*/')+'</pre>';
    }

    var jsc = document.createElement ('div');
    jsc.innerHTML = alljs;
    for (i = 0; i < c; ++i)
    {
        var e = els[i];
        e.parentNode.replaceChild (jsc.children[0], e);
    }

    records.style.visibility = "visible";
}

function searchHighlight (html, term) {
    return html.replace (new RegExp (term, 'g'), function (match) {
        return '<span class="match">'+match+'</span>';
    })
}

