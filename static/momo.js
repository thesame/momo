window.addEventListener ('load', function () {
    var p = document.createElement ('p');
    p.innerHTML = '<form method="post"><input type="hidden" name="confirm" value="1"></form>';
    var poster = p.firstChild;

    document.getElementsByClassName ('confirm').toArray().forEach (function (e) {
        e.onclick = function () {
            if (confirm ("Are you sure?"))
            {
                poster.setAttribute ('action', this.getAttribute ('href'));
                poster.submit();
            }
            return false;
        }
    })
});

Node.prototype.addClass = function (className) {
    this.className += (' '+className);
    return this;
}

Node.prototype.removeClass = function (className) {
    this.className = this.className.replace (new RegExp ('\\b'+className+'\\b'), '');
    return this;
}

Node.prototype.toggleClass = function (className) {
    var c = this.className;
    this.removeClass (className);
    if (this.className == c)
        this.addClass (className);
    return this;
}

NodeList.prototype.toArray =
HTMLCollection.prototype.toArray =
function () {
    var c = this.length
        , res = [];
    for (var i = 0; i < c; ++i)
        res[i] = this[i];
    return res;
}
