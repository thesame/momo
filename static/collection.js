window.addEventListener ('load', function () {
    var records = document.getElementsByClassName ('record').toArray();
    var c = records.length;

    for (i = 0; i < c; ++i) {
        var e = records[i];
        var pre = e.getElementsByClassName ('content')[0];
        var hdr = e.getElementsByClassName ('hdr')[0];
        var p = document.createElement('p');
        p.innerHTML = '<span class="btn">...</span>';
        var btn = p.firstChild;

        hdr.insertBefore (btn, hdr.firstChild);
        hdr.style.visibility = 'hidden';

        if (c > 1)
            pre.addClass ('collapse');

        pre.onclick = function () { this.removeClass ('collapse'); };
        (function (hdr, pre) {
            btn.onclick = function () { pre.toggleClass ('collapse'); };
            e.onmouseover =  function () { hdr.style.visibility = 'inherit'; };
            e.onmouseout = function () { hdr.style.visibility = 'hidden'; };
        })(hdr, pre);
    }
})
