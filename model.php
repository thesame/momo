<?php

class MomoException extends Exception {};

if (!class_exists ('MongoClient'))
{
    class MongoClient extends Mongo
    {
        public static function
        getConnections ()
        {
            return array();
        }
    };
}

class MongoExport extends IteratorIterator implements Iterator, Countable
{
    public function
    current()
    {
        return MomoModel::morph (parent::current());
    }

    public function
    count ()
    {
        return $this->getInnerIterator()->count (true);
    }
}

class MomoModel //momomomomomodel
{
    private $conns = array();
    private $config;

    private static $coll_name_re = '(?!system\.)[^\$]+';

    private function
    conn ($server)
    {
        if (!isset ($conns[$server]))
        {
            $conns[$server] = @new MongoClient ($this->config[$server]['conn'], array('connect' => true));
        }
        return $conns[$server];
    }

    public function
    __construct ($config)
    {
        $this->config = $config;
    }

    public function
    get_servers ()
    {
        return array_keys ($this->config);
    }

    public function
    get_dbs ($server)
    {
        if (!isset ($this->config[$server]))
            throw new MomoException ('No such server');
        
        if ($this->config[$server]['db'] == '*')
        {
            $t = $this->conn ($server)->listDBs();
            $dbs = $t['databases'];
        }
        else
        {
            $t = explode (',', $this->config[$server]['db']);
            $dbs = array_map (function ($e) { return array ('name' => $e, 'sizeOnDisk' => -1, 'empty' => false); }, $t);
        }

        return $dbs;
    }

    public function
    get_collections ($server, $db)
    {
        if (method_exists ($this->conn ($server)->$db, 'getCollectionNames'))
        {
            $mdb = $this->conn ($server)->$db;
            $mdb->setSlaveOkay (true);
            return $mdb->getCollectionNames();
        }
        $ret = $this->conn ($server)->$db->execute ('db.getCollectionNames()');
        if ($ret['ok'])
            return $ret['retval'];
        throw new MomoException ($ret['errmsg']);
    }

    protected function
    get_cursor ($host, $db, $coll, $req, $limit = null, $offset = null)
    {
        $mdb = $this->conn ($host)->$db;
        $mdb->setSlaveOkay (true);
        $mcoll = $mdb->$coll;
        $req = self::unmorph ($req);
        $res = $mcoll->find ($req);
        if ($limit !== null)
            $res = $res->limit ($limit);
        if ($offset !== null)
            $res = $res->skip ($offset);
        return $res;
    }

    static function
    morph ($src)
    {
       /**
        * This trick allows us to pass strings and ints to foreach without
        * checking their type before. This gives huge performance boost. I feel
        * sorry about it.
        * */
        $err_rep = error_reporting();
        error_reporting ($err_rep & ~E_WARNING);

        $queue = [&$src];

        for ($i = 0; isset ($queue[$i]); ++$i)
        {
            foreach ($queue[$i] as $key => &$val)
            {
                if (empty ($val)) //whatever it is
                    continue;

                if ($val instanceof MongoId)
                    $val = array('$oid' => $val->{'$id'});
                elseif ($val instanceof MongoDate)
                    $val = array('$date' => (int)($val->sec*1000 + $val->usec/1000));
                elseif ($val instanceof MongoTimestamp)
                    $val = array('$timestamp' => array('t' => $val->sec, 'i' => $val->inc));
                elseif ($val instanceof MongoBinData)
                    $val = array('$binary' => base64_encode ($val->bin), '$type' => $val->type);
                elseif ($val instanceof MongoMinKey)
                    $val = array('$minkey' => 1);
                elseif ($val instanceof MongoMaxKey)
                    $val = array('$maxkey' => 1);
                else/*if (is_array ($val))*/
                    $queue[] = &$val;
            }
            unset ($queue[$i]);
        }
        error_reporting ($err_rep);
        return $src;
    }

    public function
    fetch ($host, $db, $coll, $req, $limit = null, $offset = null)
    {
        return new MongoExport ($this->get_cursor ($host, $db, $coll, $req, $limit, $offset));
    }

    public function
    count_records ($host, $db, $coll, $req, $limit = null, $offset = null)
    {
        $c = $this->get_cursor ($host, $db, $coll, $req);
        return $c->count (true);
    }

    public function
    find_record ($host, $db, $coll, $id)
    {
        $mcoll = $this->conn ($host)->$db->$coll;
        $rec = $mcoll->findOne (array('_id' => new MongoId ($id)));
        $rec = self::morph ($rec);
        return $rec;
    }

    protected static function
    unmorph ($rec)
    {
        $err_rep = error_reporting(); //look self::morph
        error_reporting ($err_rep & ~E_WARNING);

        $queue = [&$rec];
        for ($i = 0; isset ($queue[$i]) || array_key_exists ($i, $queue); ++$i)
        {
            foreach ($queue[$i] as $key => &$val)
            {
                if (isset ($val['$oid']) && count ($val) == 1)
                {
                    $val = new MongoId ($val['$oid']);
                }
                elseif (isset ($val['$date']) && count ($val) == 1)
                {
                    $val = new MongoDate (
                        (int)($val['$date'] / 1000),
                        ($val['$date'] % 1000) * 1000
                        );
                }
                elseif (isset ($val['$timestamp']) &&
                    isset ($val['$timestamp']['t']) &&
                    isset ($val['$timestamp']['i']) &&
                    count ($val) == 1 &&
                    count ($val['timestamp']) == 2)
                {
                    $val = new MongoTimestamp (
                        $val['$timestamp']['t'],
                        $val['$timestamp']['i']
                        );
                }
                elseif (isset ($val['$binary']) &&
                    isset ($val['$type']) &&
                    count ($val) == 2)
                {
                    $val = new MongoBinData (
                        base64_decode ($val['$binary']),
                        $val['$type']
                        );
                }
                elseif (isset ($val['$minkey']) && count ($val) == 1)
                {
                    $val = new MongoMinKey();
                }
                elseif (isset ($val['$maxkey']) && count ($val) == 1)
                {
                    $val = new MongoMaxKey();
                }
                else
                    $queue[] = &$val;
            }
            unset ($queue[$i]);
        }
        error_reporting ($err_rep);
        return $rec;
    }

    public function
    update_record ($host, $db, $coll, $id, $data)
    {
        $data = self::unmorph ($data);
        $this->conn ($host)->$db->$coll->update (array('_id' => new MongoId ($id)), $data);
    }

    public function
    insert_record ($host, $db, $coll, $data)
    {
        $data = self::unmorph ($data);
        $this->conn ($host)->$db->$coll->insert ($data);
        return (string)$data['_id'];
    }

    public function
    delete_record ($host, $db, $coll, $id)
    {
        $this->conn ($host)->$db->$coll->remove (array('_id' => new MongoId ($id)));
    }

    public static function
    get_mongo_info ()
    {
        return self::return_stats (array(
            'libversion' => MongoClient::VERSION,
            'allow_empty_keys' => !!ini_get ('mongo.allow_empty_keys'),
            'connections' => implode ('<br/>', array_map (function ($e) {return $e['hash'];}, MongoClient::getConnections())),
            'chunk_size' => ini_get ('mongo.chunk_size'),
        ), self::$server_stats_map);
    }

    public function
    get_server_info ($host)
    {
        $mng = $this->conn ($host);
        $hosts = implode (
                '<br/>',
                array_map (function ($e) {
                    return "{$e['host']}:{$e['port']} ".(isset ($e['ping']) ? "({$e['ping']}ms)" : '');
                }, $mng->getHosts()));
        return array(
            'Hosts' => ['raw' => $hosts, 'text' => $hosts]
        );
    }
    
    protected static $db_stats_map = array(
        'db' => 'Database Name',
        'collections' => 'Count of Collections',
        'objects' => 'Count of Objects',
        'avgObjSize' => 'Average Object Size',
        'dataSize' => 'Data Size',
        'storageSize' => 'Storage Size',
        'numExtents' => 'Number of Extents',
        'indexes' => 'Number of Indexes',
        'indexSize' => 'Index Size',
        'fileSize' => 'File Size',
        'nsSizeMB' => 'Namespace Files Size (MiB)',
    );
    protected static $coll_stats_map = array(
        'ns' => 'Namespace',
        'count' => 'Numeber of Documents',
        'size' => 'Size',
        'avgObjSize' => 'Average Object Size',
        'storageSize' => 'Allocated Storage Size',
        'numExtents' => 'Number of Extents',
        'nindexes' => 'Number of Indexes',
        'lastExtentSize' => 'Size of last created extent',
        'paddingFactor' => 'Padding Factor',
        'flags' => 'Flags',
        'totalIndexSize' => 'Total Index Size',
    );

    protected static $server_stats_map = [
        'libversion' => 'MongoClient Version',
        'allow_empty_keys' => 'Empty Keys Allowed',
        'connections' => 'Established Connections',
        'chunk_size' => 'Chunk Size'
    ];

    protected static function
    return_stats ($stats, $map)
    {
        $res = array();
        foreach ($map as $key => $title)
        {
            if (!isset ($stats[$key]))
                continue;
            $v = $stats[$key];
            if (0 == strcasecmp (substr ($key, -4), 'size'))
            {
                $factor = 1;
                foreach (array('B', 'KiB', 'MiB', 'GiB') as $scale)
                {
                    if ($v < $factor*1024)
                    {
                        $t = $v / $factor;
                        $frac = $t - floor ($t);
                        if ($frac == 0)
                            $v = $t.' '.$scale;
                        else
                            $v = sprintf ('%.2f %s', $t, $scale);
                        break;
                    }
                    $factor *= 1024;
                    continue;
                }
            }
            if ($v === true)
                $v = 'yes';
            if ($v === false)
                $v = 'no';
            $res[$title] = ['text' => $v, 'raw' => $stats[$key]];
        }
        return $res;
    }

    public function
    get_db_info ($host, $db)
    {
        $mdb = $this->conn ($host)->{$db};
        $stats = $mdb->command (array('dbStats' => 1));
        return $this->return_stats ($stats, self::$db_stats_map);
    }

    public function
    get_collection_info ($host, $db, $coll)
    {
        $mdb = $this->conn ($host)->{$db};
        $stats = $mdb->command (array('collStats' => $coll));
        return $this->return_stats ($stats, self::$coll_stats_map);
    }

    public function
    empty_collection ($host, $db, $coll)
    {
        $mdb = $this->conn ($host)->$db->$coll->remove (array(), array('justOne' => false));
    }

    public function
    export_records ($records, $output)
    {
        $res = '';
        foreach ($records as $rec)
            fputs ($output, json_encode ($rec)."\r\n");
    }

    public function
    export_db ($host, $db, $filename)
    {
        $colls = $this->get_collections ($host, $db);
        $zip = new ZipArchive;
        $ferr = $zip->open ($filename, ZipArchive::CREATE);
        if ($ferr !== true)
            throw new MomoException ("File error $ferr");

        $names = [];
        foreach ($colls as $coll)
        {
            $recs = $this->fetch ($host, $db, $coll, array());
            $tempname = tempnam (sys_get_temp_dir(), "momo-$db-$coll");
            $names[] = $tempname;
            $f = fopen ($tempname, 'w');
            $exp = $this->export_records ($recs, $f);
            fclose ($f);
            $zip->addFile ($tempname, "$coll.json");
        }
        $zip->close();
        foreach ($names as $tempname)
            unlink ($tempname);
    }

    const IMPORT_EMPTY = 1; // < Empty collection before import
    const IMPORT_EXISTANT = 2; // < Import only existant collections
    const IMPORT_DRY_RUN = 4; // < Dry run (no actual changes made)

    public function
    import_db ($filename, $host, $db, $flags = 0)
    {
        $zip = new ZipArchive;
        if (true !== $zip->open ($filename))
            throw new MomoException ('Could not open zip file');

        for ($i = 0; $i < $zip->numFiles; ++$i)
        {
            $fname = $zip->getNameIndex ($i);
            if (!preg_match ('/^'.self::$coll_name_re.'\.json$/', $fname))
                throw new MomoException ('Invalid name');
        }

        for ($i = 0; $i < $zip->numFiles; ++$i)
        {
            $fname = $zip->getNameIndex ($i);
            $coll = basename ($fname, '.json');
            if (($flags & self::IMPORT_EXISTANT) && !$this->collection_exists ($host, $db, $coll))
                continue;

            if ($flags & self::IMPORT_EMPTY && !($flags & self::IMPORT_DRY_RUN))
                $this->empty_collection ($host, $db, $coll);

            $stream = $zip->getStream ($fname);
            $this->import_collection ($host, $db, $coll, $stream, !!($flags & self::IMPORT_DRY_RUN));
        }
        $zip->close();
    }

    public function
    drop_db ($host, $db)
    {
        $this->conn ($host)->$db->drop();
    }

    public function
    grep_db ($host, $db, $q)
    {
        $res = array();
        $colls = $this->get_collections ($host, $db);
        $count = 0;
        foreach ($colls as $coll)
        {
            $recs = $this->fetch ($host, $db, $coll, array());
            foreach ($recs as $id => $rec)
            {
                $json = json_encode ($rec);
                if (preg_match ("/$q/", $json))
                {
                    $res[$coll][$id] = $rec;
                    if (++$count >= 50) break;
                }
            }
            if ($count >= 50) break;
        }
        return $res;
    }

    public function
    collection_exists ($host, $db, $coll)
    {
        return in_array ($coll, $this->get_collections ($host, $db));
    }

    public function
    import_collection ($host, $db, $coll, $source, $dry_run = false, $overwrite = false)
    {
        $mdb = $this->conn ($host)->$db->$coll;
        $line = 0;
        $report = ['processed' => 0, 'overwritten' => 0];
        while (($s = fgets ($source)))
        {
            $line += 1;
            $record = json_decode ($s, true);
            if (json_last_error() != JSON_ERROR_NONE)
                throw new MomoException ("Error while processing line $line: ".json_last_error());
            $record = self::unmorph ($record);
            if ($dry_run)
                continue;

            if ($overwrite)
            {
                $ret = $mdb->update (['_id' => $record['_id']], $record, ['upsert' => true]);
            }
            else
            {
                $ret = $mdb->insert ($record, array('w' => true));
            }
            if ($ret['updatedExisting'])
                $report['overwritten']++;

            if (!empty ($ret['err']))
                throw new MomoException ("Error while processing line $line: {$ret['err']}");
            $report['processed']++;
        }
        return $report;
    }

    public function
    drop_collection ($host, $db, $coll)
    {
        $this->conn ($host)->$db->$coll->drop();
    }

    public function
    get_host_color ($host)
    {
        return $this->config[$host]['color'];
    }

    public function
    get_host_colors ()
    {
        return array_map (function ($e) { return $e['color']; }, $this->config);
    }
    
    public function
    create_collection ($host, $db, $name)
    {
        if (!preg_match ('/^'.self::$coll_name_re.'$/', $name))
            throw new MomoException ('Invalid Collection Name');
        $this->conn ($host)->$db->createCollection ($name);
        return true;
    }

    public function
    id_search ($host, $db, $id)
    {
        $colls = $this->get_collections ($host, $db);
        $req = ['_id' => ['$oid' => $id]];
        foreach ($colls as $coll)
        {
            $recs = iterator_to_array ($this->fetch ($host, $db, $coll, $req));
            if (count ($recs) == 0)
                continue;
            return array ($coll, $recs);
        }
        return null;
    }
}
