<?php

class Application
{
    public function
    __construct ($model, $base_url, $url, $cfg, $args)
    {
        $this->mdl = $model;
        $this->base_url = $base_url;
        $this->url = $url;
        $this->cfg = $cfg;
        $this->args = $args;
    }

    public function
    run ($dest, $args)
    {
        try {
            $resp = call_user_func_array (array($this, $dest), $args);
        } catch (Exception $e) {
            $resp = $this->on_exception ($e);
        }
        echo $resp;
    }

    public static function
    get_routes ()
    {
        return array (
            '/' => 'home',
            '/$host' => 'server',
            '/$host/$db' => 'db',
            '/$host/$db/$collection' => 'collection_view',
            '/$host/$db/$collection/$id' => 'record_view',
            '/$host/$db/\*/$id' => 'id_search',
            '/$host/$db/$collection/post' => 'record_post',
            '/$host/$db/$collection/drop' => 'collection_drop',
            '/$host/$db/$collection/stats' => 'collection_stats',
            '/$host/$db/$collection/export' => 'collection_export',
            '/$host/$db/$collection/import' => 'collection_import',
            '/$host/$db/$collection/empty' => 'collection_empty',
            '/$host/$db/$collection/$id/edit' => 'record_post',
            '/$host/$db/$collection/$id/delete' => 'record_delete',
            '/$host/$db/\+new' => 'collection_new',
            '/$host/$db/\+export' => 'db_export',
            '/$host/$db/\+import' => 'db_import',
            '/$host/$db/\+drop' => 'db_drop',
            '/$host/$db/\+grep' => 'db_grep',
            '/$host/$db/\+details' => 'db_details',
            '/cfg/edit/$host' => 'cfg_edit',
            '/cfg/delete/$host' => 'cfg_del',
            '/cfg/add' => 'cfg_add',
        );
    }

    protected function
    cfg ($host, $bc)
    {
        if (!$this->cfg->is_changable())
            throw new Exception ('Instance not configurable');

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            if (!match ($_POST, array('name' => null, 'conn' => null, 'db' => null, 'black' => null)))
                die('invalid args');
            $data = $_POST; unset ($data['name']);
            $name = $_POST['name'];
            $this->cfg->mongos = array_merge ($this->cfg->mongos, array($name => $data));
            $this->cfg->persist();
            $this->redirect ("/$name");
            return;
        }
        if ($host)
        {
            $cfg = $this->cfg->mongos[$host];
            $cfg['name'] = $host;
        }
        else
            $cfg = array(
                'name' => 'localhost',
                'conn' => 'mongodb://localhost/',
                'db' => '*',
                'color' => 'green'
            );
        return render ('views/cfg_add', array(
            'cfg' => $cfg,
            'bc' => $bc
        ) + $this->generic());
    }

    public function
    cfg_add ()
    {
        return $this->cfg (null, " / <a href='{$this->base_url}/cfg/add'>New Connection</a>");
    }

    public function
    cfg_edit ($host)
    {
        return $this->cfg ($host,
            " / <a href='{$this->base_url}/{$host}'>{$host}</a>
            / <a href='{$this->base_url}/cfg/edit/{$host}'>Edit Connection</a>");
    }

    protected function
    redirect ($url)
    {
        header ("Location: {$this->base_url}$url");
        die();
    }

    public function
    cfg_del ($host)
    {
        if (!$this->cfg->is_changable())
            throw new Exception ('Instance not configurable');

        if ($this->confirm ("Are you sure want to delete server $host from Momo's config?"))
        {
            $t = $this->cfg->mongos;
            unset ($t[$host]);
            $this->cfg->mongos = $t;
            $this->cfg->persist();
            $this->redirect ('');
        }
    }

    protected function
    render_tree ($spec_server = null, $spec_db = null)
    {
        $servers = $this->mdl->get_servers();
        $res = array();
        foreach ($servers as $server)
        {
            $res[$server] = array();
            if ($server == $spec_server)
            {
                try {
                    $dbs = $this->mdl->get_dbs ($server);
                } catch (MongoException $e)
                {
                    continue;
                }
                usort ($dbs, function ($a, $b) { return strnatcasecmp ($a['name'], $b['name']); });

                if ($spec_db)
                {
                    foreach ($dbs as &$db)
                        if ($db['name'] == $spec_db)
                        {
                            $colls = $this->mdl->get_collections ($server, $spec_db);
                            natcasesort ($colls);
                            $db['colls'] = $colls;
                        }
                }
                $res[$server] = $dbs;
            }
        }
        return render ('views/tree', array(
            'tree' => $res,
            'base' => $this->base_url,
            'hostcolors' => $this->mdl->get_host_colors(),
            'configurable' => $this->cfg->is_changable()
        ));
    }

    protected function
    render_breadcrumb ()
    {
        $parts = func_get_args();
        $res = " ";
        $prefix = $this->base_url;
        foreach ($parts as $idx => $arg)
        {
            if ($arg === null)
                break;
            $prefix .= "/$arg";
            if ($idx == 0)
                $res .= "/ <a href='$prefix' style='color: ".$this->mdl->get_host_color($arg).";'>$arg</a> ";
            else
                $res .= "/ <a href='$prefix'>$arg</a> ";
        }
            
        return $res;
    }

    protected function
    get_title ($parts)
    {
        $res = empty ($parts) ? 'Welcome / ' : '';
        foreach ($parts as $idx => $arg)
        {
            if (is_integer ($idx))
                continue;
            if ($arg === null)
                break;
            $res .= $arg.' / ';
        }
        return substr ($res, 0, -3).' &ndash; MOMO';
    }

    protected function
    generic ()
    {
        extract ($this->args);
        return array(
            'tree' => $this->render_tree (@$host, @$db),
            'bc' => $this->render_breadcrumb (@$host, @$db, @$collection, @$id),
            'base' => $this->base_url,
            'prefix' => $this->base_url.$this->url,
            'title' => $this->get_title ($this->args),
            'db_prefix' => $this->base_url.'/'.@$host.'/'.@$db,
        );
    }

    public function
    on_exception (Exception $exception)
    {
        if (true || $exception instanceof MomoException)
        {
            return render ('views/error', array(
                'title' => "Error: {$exception->getMessage()} &ndash; MOMO",
                'message' => $exception->getMessage()
            ) + $this->generic ());
        }
        else
            throw $exception;
    }

    public function
    server ($host)
    {
        try {
            $info = $this->mdl->get_server_info ($host);
        } catch (MongoException $e)
        {
            $info = array('Error' => ['raw' => $e->getMessage(), 'text' => $e->getMessage()]);
        }
        $options = $this->cfg->is_changable() ?
            array(
                'Edit' => $this->base_url.'/cfg/edit/'.$host,
                'Delete'=>$this->base_url.'/cfg/delete/'.$host
            ) : array();
        return render ('views/home', array(
            'info' => $info,
            'options' => $options,
        ) + $this->generic ());
    }

    public function
    home ()
    {
        $info = $this->mdl->get_mongo_info();
        return render ('views/home', array('info' => $info) + $this->generic());
    }

    public function
    db ($host, $db)
    {
        $info = $this->mdl->get_db_info ($host, $db);
        $options = array(
            'Add Collection' => $this->base_url."/$host/$db/+new",
            'Import' => $this->base_url."/$host/$db/+import",
            'Export' => $this->base_url."/$host/$db/+export",
            'Drop' => $this->base_url."/$host/$db/+drop",
            'Details' => $this->base_url."/$host/$db/+details",
        );
        return render ('views/home', array('info' => $info, 'options' => $options) + $this->generic ());
    }

    public function
    collection_export ($host, $db, $collection)
    {
        header ('Content-Type: text/json');
        header ("Content-Disposition: attachment; filename=$collection.json");
        $output = fopen ('php://output', 'wt');
        $this->mdl->export_records ($this->mdl->fetch ($host, $db, $collection, array()), $output);
        fclose ($output);
    }

    public function
    db_export ($host, $db)
    {
        $zip = tempnam (sys_get_temp_dir(), 'momo-export-'.$db).'.zip';
        $this->mdl->export_db ($host, $db, $zip);
        header ('Content-Type: application/zip');
        header ("Content-Disposition: attachment; filename=$db.zip");
        header ('Content-Length: '.filesize ($zip));
        readfile ($zip);
    }

    public function
    db_import ($host, $db)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $error = null;

            if (@$_FILES['file']['error'] != UPLOAD_ERR_OK)
                $error = 'Upload error';
            else
            {
                try {
                    $this->mdl->import_db ($_FILES['source']['tmp_name'], $host, $db, MomoModel::IMPORT_EMPTY);
                } catch (MomoException $e)
                {
                    $error = $e->getMessage();
                }
            }
            if ($error)
                return render ('views/import_db', array('error' => $error, 'db' => $db) + $this->generic());
            else
                return $this->redirect ("/$host/$db");
        }
        else
            return render ('views/import_db', array('db' => $db) + $this->generic());
    }

    public function
    db_drop ($host, $db)
    {
        if ($this->confirm ("Are you sure want to drop database $db?"))
        {
            $record = $this->mdl->drop_db ($host, $db);
            $this->redirect ("/$host");
        }
    }

    public function
    db_grep ($host, $db)
    {
        $matches = $this->mdl->grep_db ($host, $db, $_GET['q']);
        return render ('views/grep', array (
            'matches' => $matches,
            'query' => $_GET['q']) + $this->generic());
    }

    public function
    db_details ($host, $db)
    {
        $info = $this->mdl->get_db_info ($host, $db);
        $coll_names = $this->mdl->get_collections ($host, $db);
        $colls = [];
        foreach ($coll_names as $name)
            $colls[] = $this->mdl->get_collection_info ($host, $db, $name);
        return render ('views/details', ['info' => $info, 'colls' => $colls] + $this->generic());
    }

    public function
    collection_import ($host, $db, $collection)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            if (@$_FILES['file']['error'] != UPLOAD_ERR_OK)
            {
                return render ('views/import', array('error' => 'Upload Error') + $this->generic());
                return;
            }
            $f = fopen ($_FILES['file']['tmp_name'], 'rt');
            $report = $this->mdl->import_collection ($host, $db, $collection, $f, false, isset ($_POST['overwrite']));
            fclose ($f);
            $_SESSION['message'] = "Collection $collection imported; {$report['processed']} records processed in total, {$report['overwritten']} records overwritten";
            $this->redirect ("/$host/$db/$collection/import");
        }
        return render ('views/import', $this->generic());
    }

    protected function
    confirm ($msg)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && @$_POST['confirm'])
            return true;

        echo render ('views/confirm', array(
            'cancel' => $_SERVER['HTTP_REFERER'],
            'question' => $msg,
            'context' => $_GET
        ) + $this->generic());
        return false;
    }

    public function
    collection_empty ($host, $db, $collection)
    {
        if ($this->confirm ("Are you sure want to empty collection $collection?"))
        {
            $record = $this->mdl->empty_collection ($host, $db, $collection);
            $this->redirect ("/$host/$db/$collection");
        }
    }

    public function
    collection_view ($host, $db, $collection)
    {
        $sticky = array ();
        $q = @$_GET['q'];
        if (!$q)
            $q = '{}';
        else
            $sticky['q'] = $q;

        $req = json_decode ($q, true);

        $limit = isset ($_GET['limit']) ? $_GET['limit'] : 10;
        $sticky['limit'] = $limit;
        if ($limit == 'all')
            $limit = null;
        $limit = (int)$limit;

        $offset = isset ($_GET['offset']) ? (int)$_GET['offset'] : 0;

        if (isset ($_REQUEST['action']) && $_REQUEST['action'] == 'export')
        {
            $records = $this->mdl->fetch ($host, $db, $collection, $req);
            $output = fopen ('php://output', 'wt');
            header ('Content-Type: text/json');
            header ("Content-Disposition: attachment; filename=$collection.json");
            $this->mdl->export_records ($records, $output);
            fclose ($output);
        }
        else
        {
            $records = $this->mdl->fetch ($host, $db, $collection, $req, $limit, $offset);
            $overall = $this->mdl->count_records ($host, $db, $collection, $req);
            $count = count ($records);

            $pages = $limit ?  (int) ceil ($overall / $limit) : 1;
            $page = $limit ?  $offset / $limit : 1;
            return render ('views/collection', array(
                'records' => $records,
                'overall' => $overall,
                'count' => $count,
                'perpage' => $limit,
                'page' => $page,
                'pages' => $pages,
                'collection_prefix' => $this->base_url."/$host/$db/$collection",
                'q' => $q,
                'sticky' => $sticky,
            ) + $this->generic());
        }
    }

    public function
    record_view ($host, $db, $collection, $id)
    {
        $record = $this->mdl->find_record ($host, $db, $collection, $id);
        return render ('views/record', array(
            'record' => $record,
            '_id' => $id,
            'collection_prefix' => $this->base_url."/$host/$db/$collection",
        ) + $this->generic());
    }

    public function
    record_post ($host, $db, $collection, $id = null)
    {
        if (!@$id) $id = null;

        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $data = json_decode ($_POST['record'], true);
            if (json_last_error() != JSON_ERROR_NONE)
            {
                throw new Exception (
                    'JSON error '.json_last_error().
                    (function_exists ('json_last_error_msg') ? (' ('.json_last_error_msg().')') : '')
                );
            }
            if ($id)
                $this->mdl->update_record ($host, $db, $collection, $id, $data);
            else
                $id = $this->mdl->insert_record ($host, $db, $collection, $data);
            $this->redirect ("/$host/$db/$collection/$id");
        }

        $record = $id ?
            $this->mdl->find_record ($host, $db, $collection, $id)
            : new stdclass();
        return render ('views/edit', array(
            'id' => $id,
            'record' => json_encode ($record, JSON_PRETTY_PRINT),
        ) + $this->generic());
    }

    public function
    record_delete ($host, $db, $collection, $id)
    {
        if ($this->confirm ("Are you sure want to delete record of id \"$id\" in collection \"$collection\"?"))
        {
            $record = $this->mdl->delete_record ($host, $db, $collection, $id);
            $this->redirect ("/$host/$db/$collection");
        }
    }

    public function
    collection_stats ($host, $db, $collection)
    {
        $info = $this->mdl->get_collection_info ($host, $db, $collection);
        return render ('views/home', array('info' => $info) + $this->generic());
    }

    public function
    collection_drop ($host, $db, $collection)
    {
        if ($this->confirm ("Are you sure want to drop collection \"$collection\"?"))
        {
            $this->mdl->drop_collection ($host, $db, $collection);
            $this->redirect ("/$host/$db");
        }
    }

    public function
    collection_new ($host, $db)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $name = $_POST['name'];
            $this->mdl->create_collection ($host, $db, $name);
            $this->redirect ("/$host/$db/$name");
        }
        return render ('views/coll_add', $this->generic());
    }

    public function
    id_search ($host, $db, $id)
    {
        list ($coll, $rec) = $this->mdl->id_search ($host, $db, $id);
        if (!$coll)
            throw new Exception ('Record not found');
        $this->args['collection'] = $coll;
        return $this->record_view ($host, $db, $coll, $id);
    }
}
