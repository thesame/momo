<?php

class Configurator
{
    private $config;
    private $default;
    private $loaded = true;
    private $filename;

    public function
    __construct ($filename, $default)
    {
        $this->filename = $filename;
        $this->default = $default;
        $this->config = @json_decode (file_get_contents ($filename), true);
        if ($this->config === null)
        {
            $this->loaded = false;
            $this->config = $this->default;
        }
    }

    public function
    persist ()
    {
        if ($this->loaded)
            return true;

        if (false === file_put_contents ($this->filename, json_encode ($this->config, JSON_PRETTY_PRINT)))
            throw new Exception ('Failed to save config. Check file permissions.');
        $this->loaded = true;
    }

    public function
    is_changable ()
    {
        return is_writable ($this->filename) && @$this->config['configurable'];
    }

    public function
    __get ($key)
    {
        return $this->config[$key];
    }

    public function
    __set ($key, $val)
    {
        $this->config[$key] = $val;
        $this->loaded = false;
    }
}
