<? include 'header.php' ?>
<table>
<? foreach ($info as $key => $val) { ?>
    <tr>
        <td class="key"><?= $key ?>:</td>
        <td class="value" title="<?= htmlspecialchars ($val['raw'], ENT_QUOTES, 'UTF-8') ?>">
            <?= $val['text'] ?>
        </td>
    </tr>
<? } ?>
</table>

<? if (@$options) foreach ($options as $option => $url) { ?>
    <a href="<?= $url ?>" class="btn <?= preg_match ('/(delete)|(drop)/i', $option) ? 'confirm' : ''?>"><?= $option ?></a>
<? } ?>
<? include 'footer.php' ?>
