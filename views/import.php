<? include 'header.php' ?>

<? if (@$error) { ?>
    <div class="error"><?= $error ?></div>
<? } ?>
<form action="" method="post" enctype="multipart/form-data">
    <label><input type="checkbox" name="overwrite"/>Overwrite existing records</label>
    Select JSON file to import:
    <input type="file" name="file" />
    <input type="submit" value="Import" />
</form>

<? include 'footer.php' ?>
