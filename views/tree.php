<ul class="tree">
<? foreach ($tree as $server => $dbs) { ?>
    <li>
    <a href="<?= $base ?>/<?= $server ?>" style="color: <?= $hostcolors[$server] ?>"><?= $server ?></a>
    <? if (!empty ($dbs)) { ?>
        <ul>
        <? foreach ($dbs as $db) { ?>
            <li>
            <a href="<?= $base ?>/<?= $server ?>/<?= $db['name'] ?>"><?= $db['name'] ?></a>
            <? if (!empty ($db['colls'])) { ?>
                <ul>
                <? foreach ($db['colls'] as $coll) { ?>
                    <li>
                        <a href="<?= $base ?>/<?= $server ?>/<?= $db['name'] ?>/<?= $coll ?>"><?= $coll?></a>
                    </li>
                <? } ?>
                </ul>
            <? } ?>
            </li>
        <? } ?>
        </ul>
    <? } ?>
    </li>
<? } ?>
</ul>
<? if ($configurable) { ?>
    <a class="btn" href="<?= $base ?>/cfg/add" title="New Connection">+</a>
<? } ?>
