<? include 'header.php' ?>

<script type="text/javascript" src="<?= $base ?>/static/collection.js"></script>

<div class="toolbar">
    <a href="<?= $collection_prefix ?>/post" class="btn">Insert</a>
    <a href="<?= $collection_prefix ?>/drop" class="btn confirm">Drop</a>
    <a href="<?= $collection_prefix ?>/export" class="btn">Export</a>
    <a href="<?= $collection_prefix ?>/import" class="btn">Import</a>
    <a href="<?= $collection_prefix ?>/empty" class="btn confirm">Empty</a>
    <a href="<?= $collection_prefix ?>/stats" class="btn">Stats</a>
</div>

<? if (isset ($q)) { ?>
<form action="">
    <textarea name="q" style="width: 400px"><?= $q ?></textarea>
    <?= sticky_hidden ($sticky, array ('q')); ?>
    <input type="submit" class="btn" value="Query" />
    <button type="submit" class="btn" name="action" value="export">Export</button>
</form>
<? } ?>

<div id="records">
<? foreach ($records as $_id => $rec) { ?>
    <div class="record <?= ($even = !@$even) ? 'even' : 'odd' ?>">
        <div class="content">
            <?= json_html ($_id, $rec) ?>
        </div>
        <div class="hdr toolbar">
        <? if ($_id) { ?>
            <a href="<?= $collection_prefix ?>/<?= $_id ?>" class="btn">#</a>
            <a href="<?= $collection_prefix ?>/<?= $_id ?>/edit" class="btn">Edit</a>
            <a href="<?= $collection_prefix ?>/<?= $_id ?>/delete" class="btn confirm">Delete</a>
        <? } ?>
        </div>
    </div>
<? } ?>
</div>

<? if ($pages > 1) { ?>
    <div class="pagination toolbar">
        <?= paginator ($page, $pages, $perpage, $sticky); ?>
    </div>
<? } ?>
Shown <?= $count ?> of <?= $overall ?> records

<form action="" method="get">
    Show
    <select name="limit" class="btn">
    <? foreach (array(1, 10, 25, 50, 100, 250, 500) as $opt) { ?>
        <option value="<?= $opt ?>" <?= $opt == $perpage ? 'selected' : '' ?>><?= $opt ?></option>
    <? } ?>
        <option value="all" <?= $perpage ? '' : 'selected' ?>>All</option>
    </select>
    records per page
    <?= sticky_hidden ($sticky, array ('limit')); ?>
    <input type="submit" value="Show" class="btn" />
</form>

<? include 'footer.php' ?>
