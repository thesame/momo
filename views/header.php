<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title><?= @$title ?></title>
    <link rel="stylesheet" type="text/css" href="<?= $base ?>/static/momo.css">
    <link rel="stylesheet" type="text/css" href="<?= $base ?>/static/fontello/css/momo.css">
    <link rel="icon" href="<?= $base ?>/static/momo.png" type="image/png">
    <script type="text/javascript" src="<?= $base ?>/static/momo.js"></script>
    <script type="text/javascript" src="<?= $base ?>/static/highlight.js"></script>

    <script type="text/javascript">
        window.addEventListener ('load', function () {
            highlight_start (<?= json_encode ($db_prefix) ?>);
        });
    </script>
</head>
<body>
<div class="breadcrumb">
    <h3><a href='<?= $base ?>'>MOMO</a><? if (@$bc) echo $bc; ?></h3>
</div>
<? if (@$tree) { ?>
<div id="sidebar">
    <?= $tree ?>
</div>
<? } ?>
<div style="margin-left: 200px; height: 100%; padding: 5px">
<? if (!empty ($_SESSION['message'])) { ?>
    <div id="message"><?= $_SESSION['message'] ?></div>
    <? unset ($_SESSION['message']); ?>
<? } ?>
