<? include 'header.php' ?>


<table class="sortable">
    <thead>
        <tr>

<?
    $columns = [];
    foreach ($colls as $coll)
        foreach ($coll as $title => $cell)
            if (!isset ($columns[$title])):
                $columns[$title] = count ($columns);
?>
            <th>
                <?= $title ?>
            </th>
<?
            endif;
?>
        </tr>
    </thead>

    <tbody>
<? foreach ($colls as $coll): ?>
        <tr>
    <? foreach ($columns as $title => $idx): ?>
            <td raw="<?= htmlspecialchars (@$coll[$title]['raw'], ENT_QUOTES, 'UTF-8');  ?>">
            <?= @$coll[$title]['text'] ?>
            </td>
    <? endforeach; ?>
<? endforeach; ?>
    </tbody>

</table>

<script type="text/javascript" charset="utf-8" src="<?= $base ?>/static/sortable.js"></script>

<? include 'footer.php' ?>

