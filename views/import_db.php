<? include 'header.php' ?>

<h2>Import database <?= $db ?>:</h2>
<? if (@$error) { ?>
    <div class="error"><?= $error ?></div>
<? } ?>
<form action="" method="post" enctype="multipart/form-data">
    <label>
        <input type="checkbox" name="empty" />
        Empty collection before import
    </label>
    <label>
        <input type="checkbox" name="existant" />
        Import only existant collections
    </label>
    <label>
        <input type="checkbox" name="dry run" />
        Dry run, no actual changes
    </label>
    <label>
        Source File
        <input type="file" name="source" />
    </label>
    <input type="submit" value="Import" class="btn" />
</form>

<? include 'footer.php' ?>
