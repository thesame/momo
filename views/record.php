<? include 'header.php' ?>

<div id="records">
    <div class="record even">
        <div class="hdr toolbar">
        <? if ($_id) { ?>
            <a href="<?= $collection_prefix ?>/<?= $_id ?>" class="btn">#</a>
            <a href="<?= $collection_prefix ?>/<?= $_id ?>/edit" class="btn">Edit</a>
            <a href="<?= $collection_prefix ?>/<?= $_id ?>/delete" class="btn confirm">Delete</a>
        <? } ?>
        </div>
        <?= json_html ($_id, $record); ?>
    </div>
</div>
<? include 'footer.php' ?>
