<? include 'header.php' ?>

<form action="" method="post" style="height: 100%; width: 100%">
    <input type="hidden" name="id" value="<?= @$id ?>" />
    <textarea style="height: 100%; width: 100%" name="record"><?= htmlentities ($record, ENT_QUOTES, 'UTF-8'); ?></textarea>
    <input type="submit" value="Save" class="btn" />
</form>

<? include 'footer.php' ?>
