<?php

if (!defined ('JSON_PRETTY_PRINT'))
    define ('JSON_PRETTY_PRINT', 0);

function
match ($a1, $a2)
{
    if (is_array ($a1) && is_array ($a2))
    {
        if (count ($a1) != count ($a2))
            return false;
        reset ($a1);
        reset ($a2);
        for ($i = 0; $i < count ($a1); ++$i)
        {
            if (key ($a1) !== key ($a2))
                return false;
            $c1 = current ($a1);
            $c2 = current ($a2);
            if ($c1 !== null && $c2 !== null && !match ($c1, $c2))
                return false;
        }
        return true;
    }
    else
        return $a1 === $a2;
}
