<?php

function
render ($tpl, $ctx = array())
{
    global $__errors;
    extract ($ctx);
    ob_start();
    include "$tpl.php";
    $c = ob_get_contents();
    ob_end_clean();
    return $c;
}

function
route ($routemap, $uri)
{
    foreach (array_reverse ($routemap) as $pattern => $route)
    {
        $pattern = preg_replace ('/(?<!\\\)\$([^\/]+)/', '(?P<\1>[^/]+)', $pattern);
        $re = str_replace ( '/',  '\/', $pattern);

        if (preg_match ("/^$re/i", $uri, $m))
        {
            array_shift ($m);
            foreach ($m as $key => $val)
                if (is_integer ($key))
                    unset ($m[$key]);
            return array($route, $m);
        }
    }
}

function
tune_urls ($base_path = null)
{
    if (!$base_path)
        $base_path = dirname ($_SERVER['SCRIPT_NAME']);
    if ($base_path == '/')
        $base_path = '';

    $url = isset ($_SERVER['REDIRECT_URL']) ? $_SERVER['REDIRECT_URL'] : $_SERVER['REQUEST_URI'];
    $parts = parse_url ($url);
    $url = substr ($parts['path'], strlen ($base_path));
    return array($base_path, $url);
}

function
paginator ($page, $pages, $perpage, $sticky = array())
{
    $tail = 3;
    $around = 4;

    $page += 1;

    unset ($sticky['limit'], $sticky['offset']);
    $sticky = "&".http_build_query ($sticky + ['limit' => $perpage]);

    $res = '';
    if ($page > 1)
        $res .= "<a href='?&offset=".(($page-2)*$perpage)."' class='btn'>&lt; Prev</a>";
    else
        $res .= '<span class="btn disabled">&lt; Prev</span>';

    for ($i = 1; $i <= min ($tail, $page-$around-1); ++$i)
        $res .= "<a href='?offset=".(($i-1)*$perpage)."$sticky' class='btn'>$i</a>";

    if ($page > $tail+$around+1)
        $res .= "<a href='?offset=".(($page-$around*2-2)*$perpage)."$sticky' class='btn'>...</a>";

    for ($i = max (1, $page-$around); $i < $page; ++$i)
        $res .= "<a href='?offset=".(($i-1)*$perpage)."$sticky' class='btn'>$i</a>";

    $res .= "<a href='?offset=".(($page-1)*$perpage)."$sticky' class='btn active'>$i</a>";

    for ($i = $page+1; $i <= min ($page+$around, $pages); ++$i)
        $res .= "<a href='?offset=".(($i-1)*$perpage)."$sticky' class='btn'>$i</a>";

    if ($page < $pages-($tail+$around))
        $res .= "<a href='?offset=".(($page+$around*2)*$perpage)."$sticky' class='btn'>...</a>";

    for ($i = max ($pages-$tail, $page+$around+1); $i <= $pages; ++$i)
        $res .= "<a href='?offset=".(($i-1)*$perpage)."$sticky' class='btn'>$i</a>";

    if ($page < $pages)
        $res .= "<a href='?offset=".(($page)*$perpage)."$sticky' class='btn'>Next &gt;</a>";
    else
        $res .= '<span class="btn disabled">Next &gt;</span>';
    return $res;
}

function
sticky_hidden ($sticky, $exclude = array())
{
    foreach ($exclude as $exvar)
        unset ($sticky[$exvar]);

    $res = '';
    foreach ($sticky as $name => $value)
        $res .= '<input type="hidden" name="'.htmlentities ($name, ENT_QUOTES, 'UTF-8').'" value="'.htmlentities ($value, ENT_QUOTES, 'UTF-8').'" />';
    return $res;
}

function
json_html ($id, $record)
{
    return "<pre class='prettyprint ".(JSON_PRETTY_PRINT ? '' : ' unpretty')."' id='$id'>"
        .htmlentities (json_encode ($record, JSON_PRETTY_PRINT), ENT_NOQUOTES, 'UTF-8')
    .'</pre>';
}
